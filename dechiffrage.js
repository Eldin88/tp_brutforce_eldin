const calculateHash = (str) => {
    let hash = str
    .split("")
    .map((c, i) => str.charCodeAt(i))
    .map((c) => c + 2)
    .map((c) => String.fromCharCode(c))
    .join("");
    return Buffer.from(hash).toString("base64");
};

const fs = require('fs'); 
const readline = require('readline');

const rl = readline.createInterface({
    input: fs.createReadStream('chiffrage.txt'),
    output: process.stdout,
    terminal: false
});

rl.on('line', (line) => {
    if (calculateHash(line) == "cnFrbg==") {
        console.log(line); 
        rl.close(); 
    }
});

